#! /usr/bin/env bash

# declare -a ABIs=("x86" "Win64")
declare -a ABIs=("x86")

# declare -a BUILD_TYPES=("Debug" "Release")
declare -a BUILD_TYPES=("Debug")

GENERATOR="Visual Studio 14"


rm -rf "build/win32"
rm -rf "bin/win32"

for ABI in "${ABIs[@]}"
do
    echo "$ABI"

    mkdir -p "build/win32/$ABI"

    if [ "$ABI" == "x86" ]; then
        GENERATOR="Visual Studio 14"
    else
        GENERATOR="Visual Studio 14 Win64"
    fi

    echo $GENERATOR       

    cmake -G"$GENERATOR" \
     -B"build/win32/$ABI" -DPLATFORM_ABI=$ABI -H.
     

    for BUILD_TYPE in "${BUILD_TYPES[@]}"
    do
    echo "$BUILD_TYPE"

        cmake --build "build/win32/$ABI" \
         --target ALL_BUILD --config $BUILD_TYPE  

        echo "-----"
    done

    echo "----------"
done

echo "done"
