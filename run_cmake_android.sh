#! /usr/bin/env bash

rm -rf "build/android" 
rm -rf  "bin/android"

mkdir -p "build/android/x86"

cmake -G"MinGW Makefiles" \
 -DCMAKE_TOOLCHAIN_FILE="cmake/android.toolchain.cmake" \
 -DCMAKE_MAKE_PROGRAM="$ANDROID_NDK/prebuilt/windows-x86_64/bin/make.exe" \
 -DCMAKE_BUILD_TYPE=Debug \
 -DANDROID_ABI=x86 \
 -DANDROID_NATIVE_API_LEVEL=19 \
 -B"build/android/x86" -H.

cmake --build "build/android/x86"
